var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Retailer = new mongoose.Schema({
    Name: String,
    User_Name: String,
    Password: String,
    Shop_Name: String,
    Address: String,
    // Cell_Number: Number,
    City: String
});
module.exports = mongoose.model('retailer', Retailer);