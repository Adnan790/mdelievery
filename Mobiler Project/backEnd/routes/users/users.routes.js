var express = require('express');
var router = express.Router();
var controller = require('./users.controller');

/* Add Rider. */
router.post('/addrider', function(req, res, next) {
    controller.add_rider(req.body).then(result => {
        return res.status(200).send({
            success: true,
            error: null,
            msg: 'Rider Added Successfully'
        })
    }, err => {
        return res.status(500).send({
            success: false,
            error: 'internal server error',
            msg: err
        })
    })
})

// app.post('/file', upload.single('file'), (req, res, next) => {
//     const file = req.file;
//     console.log(file.filename);
//     if (!file) {
//         const error = new Error('No File')
//         error.httpStatusCode = 400
//         return next(error)
//     }
//     res.send(file);
// })


router.get('/:rider', function(req, res, next) {
    controller.get_rider(req.body).then(result => {
        return res.status(200).send({
            success: true,
            rider: result
        })
    }, err => {
        return res.status(500).send({
            success: false,
            error: 'internal server error',
            msg: err
        })
    })
})

router.get('/:id', function(req, res, next) {
    controller.getriderByid(req.body, req.params.id).then(result => {
        return res.status(200).send({
            success: true,
            id: result
        })
    }, err => {
        return res.status(500).send({
            success: false,
            error: 'internal server error',
            msg: err
        })
    })
})

router.post('/login', function(req, res, next) {
    controller.login(req.body).then(result => {
        if (result === 'IncorrectPassword') {
            return res.status(400).send({
                success: false,
                error: null,
                msg: 'Incorrect Information Provided'
            })
        } else if (result === 'LoggedIn') {
            return res.status(200).send({
                success: true,
                error: null,
                msg: 'Successfully Signed IN'
            })
        }
    }, err => {
        return res.status(500).send({
            success: false,
            error: 'internal server error',
            msg: err
        })
    })
})

router.put('/:id', function(req, res, next) {
    controller.updateRider(req.body, req.params.id).then(result => {
        return res.status(200).send({
            success: true,
            error: null,
            msg: 'Successfully Updated'
        })
    }, err => {
        return res.status(500).send({
            success: true,
            error: 'Internal server error',
            msg: err
        })
    })
});


router.delete('/:id', function(req, res, next) {
    controller.deleteRider(req.body, req.params.id).then(result => {
        return res.status(200).send({
            success: true,
            error: null,
            msg: 'Rider Successfully Deleted'
        })
    }, err => {
        return res.status(500).send({
            success: false,
            error: null,
            msg: err
        })
    })
})



module.exports = router;