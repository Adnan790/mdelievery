var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var addrider = new mongoose.Schema({
    Name: String,
    CNIC: Number,
    Phone_Number: Number,
    Email: String,
    Qualification: String
});

module.exports = mongoose.model('addrider', addrider);