var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var addshop = new mongoose.Schema({
    Name: String,
    Address: String
});

module.exports = mongoose.model('addshop', addshop);