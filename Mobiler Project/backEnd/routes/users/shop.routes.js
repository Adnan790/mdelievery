var express = require('express');
var router = express.Router();
var controller = require('./users.controller');


//Add Manager:
router.post('/addshop', function(req, res, next) {
    controller.add_shop(req.body).then(result => {
        return res.status(200).send({
            success: true,
            error: null,
            msg: 'Shop Added Successfully'
        })
    }, err => {
        return res.status(500).send({
            success: false,
            error: 'internal server error',
            msg: err
        })
    })
})



router.get('/:shop', function(req, res, next) {
    controller.get_shop(req.body).then(result => {
        return res.status(200).send({
            success: true,
            manager: result
        })
    }, err => {
        return res.status(500).send({
            success: false,
            error: 'internal server error',
            msg: err
        })
    })
});



router.delete('/:id', function(req, res, next) {
    controller.deleteManager(req.body, req.params.id).then(result => {
        return res.status(200).send({
            success: true,
            error: null,
            msg: 'shop Successfully Deleted'
        })
    }, err => {
        return res.status(500).send({
            success: false,
            error: null,
            msg: err
        })
    })
})



module.exports = router;