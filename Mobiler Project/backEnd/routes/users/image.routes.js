var express = require('express');
var router = express.Router();
var controller = require('./users.controller');



router.post('/images', function(req, res, next) {
    controller.image(req.body).then(result => {
        return res.status(200).send({
            success: true,
            error: null,
            msg: 'DONE sho'
        })
    }, err => {
        return res.status(500).send({
            success: false,
            error: 'internal server error',
            msg: err
        })
    })
})

router.get('/images/:givenImageName', function(req, res, next) {
    controller.getImage(req.body, req.params.givenImageName).then(result => {
        return res.status(200).send({
            success: true,
            error: null,
            msg: 'DONE'
        })
    }, err => {
        return res.status(500).send({
            success: false,
            error: 'internal server error',
            msg: err
        })
    })
})

module.exports = router;