var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var cors = require('cors');

require('./models/users');
require('./models/addrider');
require('./models/addmanager');
require('./models/addshop');
require('./models/image');
var usersRouter = require('./routes/users/users.routes');
var managerRouter = require('./routes/users/manager.routes');
var shopRouter = require('./routes/users/shop.routes');
var imageRouter = require('./routes/users/image.routes');

var app = express();
mongoose.connect('mongodb://localhost/Mobiler', { useNewUrlParser: true }).then(result => {
    console.log('DB Is Connected');
}).catch(err => {
    console.log(err);
})


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());


app.use('/users', usersRouter);
app.use('/manager', managerRouter);
app.use('/shop', shopRouter);
app.use('/image', imageRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;