var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ImageSchema = new Schema({

    Name: String,

    Data: Buffer,

    ContentType: String

});

module.exports = mongoose.model('ImageSchema', ImageSchema);