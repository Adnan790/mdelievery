let controller = {};
var mongoose = require('mongoose');
var Rider = mongoose.model('addrider');
var Retailer = mongoose.model('retailer');
var Manager = mongoose.model('addmanger');
var Images = mongoose.model('ImageSchema');
var Shop = mongoose.model('addshop');
var bcryptHelper = require('../../helper/bcrypt')
const multer = require('multer');

controller.login = (payload) => {
    console.log('payload:', payload);
    return new Promise((resolve, reject) => {
        try {
            var Email = "admin@gmail.com";
            var Password = "admin";
            if (Email === payload.Email && Password === payload.Password) {
                resolve('LoggedIn');
            } else {
                resolve('IncorrectPassword');
            }
        } catch (err) {
            console.log(err);
            reject(err);
        }
    })
}

//storing image:
controller.image = (payload) => {
    return new Promise((resolve, reject) => {
        try {
            // var imgPath = 'D:/images/sample.jpg';
            // var imgData = fs.readFileSync(imgPath);
            Images.create({ Data: imgData, ContentType: 'image/png' }, function(err, image) {
                if (err) {
                    reject(err);
                } else {
                    resolve(image);
                }
            });
        } catch (err) {
            console.log(err);
            reject(err);

        }
    })
}

controller.getImage = (payload) => {
    return new Promise((resolve, reject) => {
        try {
            Images.findOne({ name: name }, function(err, image) {

                if (err) {
                    reject(err);
                } else {
                    resolve(image);
                }

                // return next(err);

                // res.contentType(image.contentType);

                // res.send(image.data);

            });

        } catch (err) {
            console.log(err);
        }
    })
}

//Retreving image:




// const storage = multer.diskStorage({
//     destination: (req, file, callBack) => {
//         callBack(null, 'uploads')
//     },
//     filename: (req, file, callBack) => {
//         callBack(null, `FunOfHeuristic_${file.originalname}`)
//     }
// })

// const upload = multer({ storage: storage })

//add rider
controller.add_rider = (payload) => {
    console.log('payload:', payload);
    return new Promise((resolve, reject) => {
        try {
            Rider.create(payload, function(err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }

            })
        } catch (err) {
            console.log(err);
            reject(err);

        }
    })
}

controller.get_rider = (payload) => {
    return new Promise((resolve, reject) => {
        try {
            Rider.find({}, function(err, data) {
                if (err) {
                    reject(err);
                } else {
                    return resolve(data);
                }

            }, )
        } catch (err) {
            reject(err);

        }
    })
}

controller.getriderByid = (payload, id) => {
    return new Promise((resolve, reject) => {
        try {
            Rider.findById({ _id: id }, function(err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }

            }, )
        } catch (err) {
            reject(err);

        }
    })
}

controller.updateRider = (payload, id) => {
    console.log('Paylod', payload, 'Id', id);
    return new Promise(async(resolve, reject) => {
        try {
            Rider.findOneAndUpdate({
                _id: id
            }, {
                $set: {
                    Name: payload.Name,
                    CNIC: payload.CNIC,
                    Phone_Number: payload.Phone_Number,
                    Email: payload.Email,
                    Qualification: payload.Qualification
                }
            }, {
                upsert: true
            }, function(err, user) {
                if (err) {
                    reject(err);
                } else {
                    resolve(user);
                }
            });
        } catch (error) {
            console.log(error);
            reject(error);
        }
    })
}


controller.deleteRider = (payload, id) => {
    //console.log('Payload', payload, 'ID', id)
    return new Promise((resolve, reject) => {
        Rider.findOneAndDelete({ _id: id }, function(err, data) {

            if (err) {
                return reject(err);
            } else {
                return resolve(data);
            }
        })
    })
}

//add Manager:
controller.add_manager = (payload) => {
    console.log('payload:', payload);
    return new Promise((resolve, reject) => {
        try {
            Manager.create(payload, function(err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }

            })
        } catch (err) {
            console.log(err);
            reject(err);

        }
    })
}




controller.get_manager = (payload) => {
    return new Promise((resolve, reject) => {
        try {
            Manager.find({}, function(err, data) {
                if (err) {
                    reject(err);
                } else {
                    return resolve(data);
                }

            }, )
        } catch (err) {
            reject(err);

        }
    })
}


controller.deleteManager = (payload, id) => {
    //console.log('Payload', payload, 'ID', id)
    return new Promise((resolve, reject) => {
        Manager.findOneAndDelete({ _id: id }, function(err, data) {

            if (err) {
                return reject(err);
            } else {
                return resolve(data);
            }
        })
    })
}

//Add Shop:
controller.add_shop = (payload) => {
    console.log('payload:', payload);
    return new Promise((resolve, reject) => {
        try {
            Shop.create(payload, function(err, data) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }

            })
        } catch (err) {
            console.log(err);
            reject(err);

        }
    })
}

controller.get_shop = (payload) => {
    return new Promise((resolve, reject) => {
        try {
            Shop.find({}, function(err, data) {
                if (err) {
                    reject(err);
                } else {
                    return resolve(data);
                }

            }, )
        } catch (err) {
            reject(err);

        }
    })
}


controller.deleteShop = (payload, id) => {
        console.log('Payload', payload, 'ID', id)
        return new Promise((resolve, reject) => {
            Shop.findOneAndDelete({ _id: id }, function(err, data) {

                if (err) {
                    return reject(err);
                } else {
                    return resolve(data);
                }
            })
        })
    }
    // controller.add_retailer = (payload) => {
    //     console.log('payload:', payload);
    //     return new Promise(async(resolve, reject) => {
    //         try {
    //             payload.Password = await bcryptHelper.getHash(payload.Password);
    //             Retailer.create(payload, function(err, data) {
    //                 if (err) {
    //                     reject(err);
    //                 } else {
    //                     resolve(data);
    //                 }

//             })
//         } catch (err) {
//             console.log(err);
//             reject(err);

//         }
//     })
// }


// controller.getretailerById = (payload, id) => {
//     console.log('payload', payload, 'ID:', id);
//     return new Promise((resolve, reject) => {
//         try {
//             Retailer.findById({ _id: id }, function(err, data) {
//                 if (err) {
//                     reject(err);
//                 } else {
//                     resolve(data);
//                 }

//             }, )
//         } catch (err) {
//             reject(err);

//         }
//     })
// }

// controller.get_retailer = (payload) => {
//     return new Promise((resolve, reject) => {
//         try {
//             Retailer.find({}, function(err, data) {
//                 if (err) {
//                     reject(err);
//                 } else {
//                     return resolve(data);
//                 }

//             }, )
//         } catch (err) {
//             reject(err);

//         }
//     })
// }

module.exports = controller;