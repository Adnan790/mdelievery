var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var addmanger = new mongoose.Schema({
    Name: String,
    CNIC: Number,
    Phone_Number: Number,
    Email: String,
    Qualification: String,
    Address: String
});

module.exports = mongoose.model('addmanger', addmanger);